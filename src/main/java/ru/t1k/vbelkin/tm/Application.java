package ru.t1k.vbelkin.tm;

import ru.t1k.vbelkin.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}

